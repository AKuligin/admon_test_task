import IQueue from "../queue/IQueue";
import ClickHouse from "../clickHouse/index";
import Queue from "../queue/Queue";
import QueueRedis from "../queue/QueueRedis";
import {InsertData, InsertRequest} from '../types/index';


export default class Buffer {

  private queue: IQueue<InsertRequest>;
  private timer: Object;
  private clickHouse: ClickHouse;

  //Максимальный объем буфера, после которого происходит запись
  private readonly limit: number;

  //Максимальное время ожидания, после которого буфер будет записан в любом случае
  private readonly timeOut: number;

  constructor(limit: number, timeOut: number) {
    this.limit = limit;
    this.timeOut = timeOut;
    this.clickHouse = new ClickHouse();
    //this.queue = new QueueRedis<InsertRequest>();
    this.queue = new Queue<InsertRequest>();
    this.initTimer();
  };

  public async push(element: InsertRequest): Promise<void> {
    await this.queue.push(element);
    if (await this.queue.getLength() > this.limit) {
      await this.send();
    }
  };

  public isReady(): boolean {
    return this.queue.isReady();
  };

  private async send(): Promise<void> {
    const data = await this.queue.free();
    await this.clickHouse.insert(data);
  };

  private async initTimer(): Promise<void> {
    this.timer = setInterval(async () => {
      if (await this.queue.getLength())
        await this.send();
    }, this.timeOut)
  };
}
import axios from 'axios';
import {InsertRequest} from '../types/index';
import development from '../config/development';

const {clickhouse: config} = development;

export default class ClickHouse {

  private ready: boolean = false;

  constructor() {
    this.ready = true;
  }

  //Обёртка над запросами к кликхаусу
  private async request(requestString: string) {
    try {
      const res = await axios({
        url: `${config.host}:${config.port}`,
        method: 'post',
        data: requestString,
        headers: {'Content-Type': 'text/plain'}
      });
      return res?.data;
    } catch (e) {
      console.error(e?.response?.data);
    }
  }

  public async insert(_insert: InsertRequest[]): Promise<void> {

    let JSONEachRow: { [table: string]: Object[] } = {};

    console.log('_insert', _insert.length);
    _insert.map((value) => {
      if (JSONEachRow[value.table]) {
        JSONEachRow[value.table] = JSONEachRow[value.table].concat(value.data);
      } else{
        JSONEachRow[value.table] = value.data;
      }
    })

    Object.keys(JSONEachRow).map(async(table) => {
      const res = await this.request(`INSERT INTO ${table} FORMAT JSONEachRow ${JSON.stringify(JSONEachRow[table])}`)
    })
  };

  //Создание таблиц из конфига clickhouse -> schema
  public async initSchemaFromConfig(): Promise<void> {
    await Promise.all(config.schema.map(async (sc) => {
      return await this.request(`CREATE TABLE IF NOT EXISTS ${sc.table} (${sc.columns}) ENGINE = Memory;`);
    }))
  }

  //Функции ниже исключительно для дебага
  public async showTableSchema(name: string): Promise<void> {
    const data = await this.request(`DESCRIBE TABLE ${name} default`)
    console.log(data);
  }

  public async isReady(): Promise<boolean>  {
    return await this.request('SELECT 1');
  }

  public async getData(name: string): Promise<Object[]> {
    return await this.request(`SELECT * FROM ${name} FORMAT JSON`)
  }

  public async showTableList(): Promise<void> {
    const data = await this.request('SHOW TABLES FROM default')
    console.log(data);
  }

  public async reload(): Promise<void> {
    console.log('RELOAD');
    await Promise.all(config.schema.map(async (sc) => {
      return await this.request(`DROP TABLE IF EXISTS ${sc.table};`);
    }))
    await this.initSchemaFromConfig();
  }

}
export default {
  redis: {
    host: process.env.REDIS_HOST || '127.0.0.1',
    port: 6379,
  },
  clickhouse: {
    host: process.env.CLICKHOUSE_HOST || 'http://127.0.0.1',
    port: 8123,
    database: 'default',
    // Описание схем таблиц которые нужно создать в CH, по хорошему использовать ORM
    schema: [
      {
        table: 'person',
        columns: `
          id UInt64,
          name String,
          email String,
          age UInt8
        `
      },
      {
        table: 'city',
        columns: `
          id UInt64,
          name String,
          population UInt16
        `
      }
    ]
  },
  buffer: {
    endpoint: {
      host: process.env.BUFFER_HOST || '127.0.0.1',
      port: parseInt(process.env.BUFFER_PORT) || 3000
    },
    timeout: parseInt(process.env.TIMEOUT) || 3000,
    sizeLimit: parseInt(process.env.SIZE_LIMIT) || 100,
  },

  //Конфиг скрипта - автоматически вставляет данные в буфер
  test: {
    endpoint: 'http://127.0.0.1:3000/push',
    //Время между вызовами
    timer: parseInt(process.env.SCRIPT_TIMER) || 10,
    //Время работы скрипта
    timeout: parseInt(process.env.SCRIPT_TIMEOUT) || 1000,
    //Количество записей за одну вставку в буффер
    insertCount: parseInt(process.env.SCRIPT_INSERT_COUNT) || 100,
  }
}
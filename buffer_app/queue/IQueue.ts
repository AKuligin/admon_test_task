export default interface IQueue<t> {
  isReady: () =>  boolean;
  //Получить все элементы очереди и удалить
  free : () => Promise<Array<t>>;

  //Вставка одного или нескольких элементов в конец очереди
  push: (element: t) => void;

  getLength: () => Promise<number>;
}
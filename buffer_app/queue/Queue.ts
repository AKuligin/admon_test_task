import IQueueMap from "./IQueue";
import {InsertRequest} from '../types/index';


export default class Queue<t> implements IQueueMap<t> {

  private queue: Array<t>;

  constructor() {
    this.queue = new Array<t>();
  }

  public isReady(): boolean {
    return true;
  }

  public free(): Promise<Array<t>> {
    const data = [...this.queue];
    this.queue = new Array<t>();
    return new Promise<Array<t>>((resolve) => resolve(data));
  }

  public getLength(): Promise<number> {
    return new Promise<number>((resolve) => resolve(this.queue.length));
  };

  public push(element: t): void {
    this.queue.push(element);
  }

}
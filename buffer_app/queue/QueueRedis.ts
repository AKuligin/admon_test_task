import * as redis from "redis";
import development from "../config/development";
import IQueue from "../queue/IQueue";
const { promisify } = require("util");
const {redis: config} = development;

export default class redisQueue<t> implements IQueue<t>{
  private readonly client;
  private connected;
  private queueName: string = 'buffer';

  //Promise обёртки над функциями Redis
  private llen;
  private lrange;
  private rpush;
  private del;

  constructor() {
    this.client = redis.createClient(config.port, config.host);
    this.client.on("error", (err) => {
        console.error( "Redis ERROR", err )
    });
    this.client.on("connect", () => {
      console.log('Connected', this.client.connected);
      this.connected = this.client.connected;
      this.llen = promisify(this.client.llen, this.queueName).bind(this.client);
      this.lrange = promisify(this.client.lrange, this.queueName).bind(this.client);
      this.rpush = promisify(this.client.rpush, this.queueName).bind(this.client);
      this.del = promisify(this.client.del, this.queueName).bind(this.client);
    });
  }

  public async free(): Promise<Array<t>> {
    console.log('Redis free');
    const data = await this.lrange(`${this.queueName}`, 0, -1);
    await this.del(`${this.queueName}`);
    return data.map(field => JSON.parse(field));
  }

  public isReady(): boolean {
    return this.client.connected;
  }

  public async getLength(): Promise<number> {
    console.log('getLength');
    return await this.llen(`${this.queueName}`);
  }

  public push(element: t): void {
    console.log('push');
    this.rpush(`${this.queueName}`, JSON.stringify(element));
  }
}
import express from 'express';
import bodyParser from 'body-parser';
import developmentConfig from '../config/development';
import ClickHouse from "../clickHouse/index";
import {InsertRequest} from "../types";
import Buffer from '../buffer/index'
const {buffer: {endpoint}} = developmentConfig;


let expressApp = express();
let ch = new ClickHouse();
let buffer = new Buffer(100, 2000);
expressApp.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));
expressApp.use(bodyParser.json());

//Простой хэлсчек
expressApp.get('/healthcheck', async (req, res, next) => {
  if(await ch.isReady() && buffer.isReady()) res.sendStatus(200)
  else  res.sendStatus(500)
});

//Эндпоинт для вставки в буффер
expressApp.post('/push', (req, res, next) => {
  try {
    //console.log('body -', req.body);
    if(req.body){
      const data : InsertRequest = req.body;
      buffer.push(data);
    }
    res.send('OK');
  } catch (error) {
    console.error(`Server method /push error - ${error}`);
    res.send('ERROR');
  }
});

//Создание схемы базы в CH (для дебага)
expressApp.get('/CHCreate', async (req, res, next) => {
  //await ch.createTable('test2', ['a UInt32']);
  await ch.initSchemaFromConfig();
  await ch.showTableList();
  await ch.showTableSchema('person');
  await ch.showTableSchema('city');
  res.send('OK');
});

//Просмотреть танные в CH (для дебага)
expressApp.get('/CHgetData', async (req, res, next) => {
  const person = await ch.getData('person');
  const city = await ch.getData('city');
  res.send({person, city});
});

//Тестовая вставка в CH (для дебага)
expressApp.get('/testInsert', async (req, res, next) => {
  await ch.insert([
    {
      table:'person',
      data: [
        {id:3121,name:'rt',email:'1',age:1},
        {id:3122,name:'ert',email:'11',age:3},
        {id:3123,name:'ert',email:'12',age:5},
        {id:3123,name:'ert',email:'12',age:5},
        {id:3124,name:'ert',email:'13',age:8},
        {id:3125,name:'rt',email:'14',age:99},
        {id:312,name:'3',email:'12',age:5},
        {id:412,name:'4',email:'13',age:8},
        {id:512,name:'5',email:'14',age:99},
      ]
    },
    {
      table:'person',
      data: [
        {id:31,name:'rt',email:'1',age:1},
        {id:32,name:'ert',email:'11',age:3},
        {id:33,name:'ert',email:'12',age:5},
        {id:33,name:'ert',email:'12',age:5},
        {id:34,name:'ert',email:'13',age:8},
        {id:35,name:'rt',email:'14',age:99},
        {id:3,name:'3',email:'12',age:5},
        {id:4,name:'4',email:'13',age:8},
        {id:5,name:'5',email:'14',age:99},
      ]
    },
    {
      table:'person',
      data: [
        {id:1,name:'1',email:'1',age:1},
        {id:2,name:'2',email:'11',age:3},
        {id:3,name:'3',email:'12',age:5},
        {id:3,name:'3',email:'12',age:5},
        {id:4,name:'4',email:'13',age:8},
        {id:5,name:'5',email:'14',age:99},
      ]
    },
    {
      table:'city',
      data: [
        {id:122,name:'qw',population:5},
        {id:134,name:'tr',population:2},
        {id:112,name:'we',population:5},
      ]
    },
  ]);
});

//Пересоздание таблиц (Очистка) в CH (для дебага)
expressApp.get('/CHreload', async (req, res, next) => {
  await ch.reload();
  res.send('OK');
});

expressApp.listen(endpoint.port, () => {
  console.info('listen port', endpoint.port);
})
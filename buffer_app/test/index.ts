import {Person, City, InsertRequest} from '../types/index';
import axios from 'axios';
import development from "../config/development";
import {stringify} from "querystring";

const {test: config} = development;

function randomInt(max: number): number {
  return Math.floor(Math.random() * Math.floor(max))
}

async function insert() {
  try {
    const table = ['person', 'city'][randomInt(2)]
    let data : Person[] | City[];
    if (table === 'person') {
      let _data: Person[] = [];
      for (let i = 0; i < config.insertCount; i++) {
        _data.push({
          id: randomInt(100),
          name: randomInt(100).toString(),
          email: `${randomInt(100).toString()}@gmail.com`,
          age: randomInt(100)
        });
      }
      data = _data;
    }

    if (table === 'city') {
      let _data: City[] = [];
      for (let i = 0; i < config.insertCount; i++) {
        _data.push({
          id: randomInt(100),
          name: randomInt(100).toString(),
          population: randomInt(100000000),
        });
      }
      data = _data;
    }


    const _insert : InsertRequest = {
      table: table,
      data: data
    }

    console.log('insert', _insert);

    const res = await axios({
      url: config.endpoint,
      method: 'post',
      data: _insert,
      headers: {'Content-Type': 'application/json'}
    });

  } catch (error) {
    console.error('TEST ERROR: ', error);
  }
}

const executor = setInterval(insert, config.timer);

setTimeout(() => {
  clearInterval(executor);
}, config.timeout)
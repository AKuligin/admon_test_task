export type InsertData = Person[] | City[];

export type InsertRequest = {
    table: string;
    data: InsertData;
}

export type Person = {
    id: number;
    name: string;
    email: string;
    age: number;
}

export type City = {
    id: number;
    name: string;
    population: number;
}